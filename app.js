// JavaScript source code
var express = require('express');
var app = express();
var crypto = require('crypto');
//var moment = require('moment');
var router = express.Router();


var ssoDomain = '"https://loginstg.astro.com.my';
var ssoPartnerPwd = 'AOTG8@123';
var desPartnerPwd = 'temp123!@#';

/* Encodes the SSO Password as in the Astro SSO IDD document
*/
function encodeSSOPwd(pwdToEncode) {
    // Create a hash for the Partner Pwd and encode in base 64
    var hash = crypto.createHash('sha256')
                .update(ssoPartnerPwd)
                .digest('base64');

    // Get the first 8 bytes of the hash create a salt & encode in base 64
    var key = new Buffer(hash.substring(0, 8), 'binary');
    var cipher = crypto.createCipheriv('des-ecb', key, '');
    var c = cipher.update(pwdToEncode, 'utf8', 'base64');
    c += cipher.final('base64');

    return c;
}

/* Generates the Signature as per the Astro SSO/DES IDD document
*/
function generateSSOSignature(timestamp, body) {

    var sigString = '';

    // Add the date time
    sigString += timestamp;

    // Concantanate the params
    var key = '';
    for (key in body)
        sigString += body[key];

    // Append the partner pass
    sigString = sigString + ssoPartnerPwd;

    console.log("String to sign" + sigString);

    // Create the hash of the sig & encode in base 64
    var hash = crypto.createHash('sha256')
                       .update(sigString)
                       .digest('base64');
    return hash;
}



function main()
{
    // Listen to requests
    // This request encodes passwords for the SSO API
    router.route('/sso/proxyLogin').get(function (request, response) {

        var sig = '';
        var timestamp = moment().utc().format('YYYYMMDDHHmmss');

        var sigInfo = {
            "sig": "",
            "timestamp": ""
        };

        sigInfo.sig = sig;
        sigInfo.timestamp = timestamp;

        var body = {
            "language": "",
            "partnerKey": "",
            "password": "",
            "username": ""
        };

        body.language = request.query.language.toString();
        body.partnerKey = request.query.partnerKey.toString();
        body.password = encodeSSOPwd(request.query.password.toString());
        body.username = request.query.username.toString();

        sig = generateSignature(body);

        response.send(sigInfo);
    })

    // Verbose output
        app.listen(3000, function () {
        console.log('Listening on port 3000!');
    });
}