# accedoapi
helper scripts for accedo projects

Check out the DESSigCreation branch to generate DES signatures for the body in the payload.

## Dependencies
* Node JS
* Crypto

## How to use
### Step 1
Create a JSON obj w/ the parameters as key/value pairs in the main function. Note the keys **MUST BE SORTED** alphabetically asc.
e.g if the get entitlement request is

```
#!json

		"body":{
			"language":"eng",
			"partnerkey":"des",
			"objtype":[""],
			"portaluserid":"REGULAR123456789",
			"mode":"Flat"
		}
```

then

```
#!javascript

var bodyGetEntitlement = {
        "language": "eng",
        "mode": "Flat",
        "partnerKey": "des",
        "portaluserid": "REGULAR123456789"
    };

```
### Step 2
2. Update the timestamp param in the main() func as YYYYMMDDHHmmss format as per the timestamp in the request. 
 e.g if the timestamp is 2016-05-05T01:48:08Z in the req then
 
```
#!javascript

timestamp = '20160505014808'
```

### Step 3   
3. Pass these as params to the generateDESSignature(timestamp, bodyGetEntitlement) in the main() func.